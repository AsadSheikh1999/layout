

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var calculatorWorkings: UILabel!
    @IBOutlet weak var calculatorResults: UILabel!
    
    var workings:String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        clearAll()
    }

    func clearAll() {
        workings = ""
        calculatorWorkings.text = ""
        calculatorResults.text = ""
        
    }
    
    @IBAction func allClearTab(_ sender: UIButton) {
        clearAll()
    }
    
    @IBAction func equalTab(_ sender: UIButton) {
        let expression = NSExpression(format: workings)
        let result = expression.expressionValue(with: nil, context: nil) as! Double
        let resultString = formatResult(result: result)
        calculatorResults.text = resultString
        
    }
    
    func formatResult(result: Double) -> String
    {
        if(result.truncatingRemainder(dividingBy: 1) == 0){
           return String(format: "%.0f", result)
        }
        else {
            return String(format: "%.2f", result)
        }
    }
    
    @IBAction func backTap(_ sender: UIButton) {
        if(!workings.isEmpty){
            workings.removeLast()
            calculatorWorkings.text = workings
        }
    }
    
    func addToWorking(value: String) {
        workings = workings + value
        calculatorWorkings.text = workings
        
    }
    
    @IBAction func plusMinus(_ sender: UIButton) {
        addToWorking(value: "+/-")
    }
    
    @IBAction func percentTab(_ sender: UIButton) {
        addToWorking(value: "%")
    }

    @IBAction func divideTab(_ sender: UIButton) {
        addToWorking(value: "/")
    }
    
    @IBAction func sevenTab(_ sender: UIButton) {
        addToWorking(value: "7")
    }
    
    @IBAction func eightTab(_ sender: UIButton) {
        addToWorking(value: "8")
    }
    
    @IBAction func nineTab(_ sender: UIButton) {
        addToWorking(value: "9")
    
    }
    
    @IBAction func multiplyTab(_ sender: UIButton) {
        addToWorking(value: "*")
    }
    
    @IBAction func fourTab(_ sender: UIButton) {
        addToWorking(value: "4")
    }
    
    @IBAction func fiveTab(_ sender: UIButton) {
        addToWorking(value: "5")
    }
    
    @IBAction func sixTab(_ sender: UIButton) {
        addToWorking(value: "6")
    }
    
    @IBAction func minusTab(_ sender: UIButton) {
        addToWorking(value: "-")
    }
    
    @IBAction func oneTab(_ sender: UIButton) {
        addToWorking(value: "1")
    }
    
    @IBAction func twoTab(_ sender: UIButton) {
        addToWorking(value: "2")
    }
    
    @IBAction func threeTab(_ sender: UIButton) {
        addToWorking(value: "3")
    }
    
    
    @IBAction func plusTab(_ sender: UIButton) {
        addToWorking(value: "+")
    }
    
    @IBAction func zeroTab(_ sender: UIButton) {
        addToWorking(value: "0")
    }
    
    @IBAction func decimalTab(_ sender: UIButton) {
        addToWorking(value: ".")
    }
    
  
}

